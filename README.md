# K22 - Grafana

Wrapper chart including :
- Grafana
- Prometheus (+ alertmanager, kube-state-metrics, prometheus-node-exporter, prometheus-pushgateways)
- Grafana Loki (+ promtail)
- Grafana Tempo